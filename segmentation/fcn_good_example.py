
# coding: utf-8

# https://github.com/k3nt0w/chainer_fcn  
# https://github.com/wkentaro/fcn

# In[ ]:


from __future__ import division
from __future__ import print_function
import numpy as np
import chainer
import chainer.functions as F
import chainer.links as L
from chainer import cuda, optimizers, serializers, Variable
from chainer import training
from chainer.training import extensions
from chainer.datasets.tuple_dataset import TupleDataset
import sys; sys.argv=['']; del sys

import sys
import os
import argparse
import copy


# # FCN model

# In[ ]:


import math

import numpy as np
import chainer
import chainer.links as L
import chainer.functions as F
from chainer import serializers
from chainer import Variable

class FCN(chainer.Chain):
    def __init__(self, n_class=21):
        super(FCN, self).__init__(
            conv1_1=L.Convolution2D(3, 64, 3, stride=1, pad=1),
            conv1_2=L.Convolution2D(64, 64, 3, stride=1, pad=1),

            conv2_1=L.Convolution2D(64, 128, 3, stride=1, pad=1),
            conv2_2=L.Convolution2D(128, 128, 3, stride=1, pad=1),

            conv3_1=L.Convolution2D(128, 256, 3, stride=1, pad=1),
            conv3_2=L.Convolution2D(256, 256, 3, stride=1, pad=1),
            conv3_3=L.Convolution2D(256, 256, 3, stride=1, pad=1),

            conv4_1=L.Convolution2D(256, 512, 3, stride=1, pad=1),
            conv4_2=L.Convolution2D(512, 512, 3, stride=1, pad=1),
            conv4_3=L.Convolution2D(512, 512, 3, stride=1, pad=1),

            conv5_1=L.Convolution2D(512, 512, 3, stride=1, pad=1),
            conv5_2=L.Convolution2D(512, 512, 3, stride=1, pad=1),
            conv5_3=L.Convolution2D(512, 512, 3, stride=1, pad=1),

            pool3=L.Convolution2D(256, n_class, 1, stride=1, pad=0),
            pool4=L.Convolution2D(512, n_class, 1, stride=1, pad=0),
            pool5=L.Convolution2D(512, n_class, 1, stride=1, pad=0),

            upsample4=L.Deconvolution2D(n_class, n_class, ksize= 4, stride=2, pad=1),
            upsample5=L.Deconvolution2D(n_class, n_class, ksize= 8, stride=4, pad=2),
            upsample =L.Deconvolution2D(n_class, n_class, ksize=16, stride=8, pad=4),
        )

    def __call__(self, x):
        h = F.relu(self.conv1_2(F.relu(self.conv1_1(x))))
        h = F.max_pooling_2d(h, 2, stride=2)
        h = F.relu(self.conv2_2(F.relu(self.conv2_1(h))))
        h = F.max_pooling_2d(h, 2, stride=2)
        h = F.relu(self.conv3_3(F.relu(self.conv3_2(F.relu(self.conv3_1(h))))))
        p3 = F.max_pooling_2d(h, 2, stride=2)
        h = F.relu(self.conv4_3(F.relu(self.conv4_2(F.relu(self.conv4_1(p3))))))
        p4 = F.max_pooling_2d(h, 2, stride=2)
        h = F.relu(self.conv5_3(F.relu(self.conv5_2(F.relu(self.conv5_1(p4))))))
        p5 = F.max_pooling_2d(h, 2, stride=2)

        p3 = self.pool3(p3)
        p4 = self.upsample4(self.pool4(p4))
        p5 = self.upsample5(self.pool5(p5))

        h = p3 + p4 + p5
        o = self.upsample(h)

        return o


# In[ ]:


class Classifier(chainer.Chain):
    compute_accuracy = True
    def __init__(self, predictor,
                 lossfun=F.softmax_cross_entropy,
                 accfun=F.accuracy,
                 ignore_label=None):
        super(Classifier, self).__init__(predictor=predictor)
        self.lossfun = lossfun
        self.accfun = accfun
        self.ignore_label = ignore_label
        self.y = None
        self.loss = None
        self.accuracy = None  
        
    def __call__(self, x, t):
        self.y = None
        self.loss = None
        self.accuracy = None
        self.y = self.predictor(x)
        self.loss = self.lossfun(self.y, t)
        chainer.report({'loss': self.loss}, self)
        if self.compute_accuracy:
            self.accuracy = self.accfun(self.y, t, self.ignore_label)
            chainer.report({'accuracy': self.accuracy}, self)
        return self.loss


# In[ ]:


from chainer.dataset import convert
class MyUpdater(chainer.training.StandardUpdater):

    def __init__(self, iterator, optimizer, 
                 converter=convert.concat_examples, device=None):
        if isinstance(iterator, chainer.dataset.iterator.Iterator):
            iterator = {'main':iterator}
        self._iterators = iterator

        if isinstance(optimizer, chainer.optimizer.Optimizer):
            optimizer = {'main': optimizer}
        self._optimizers = optimizer

        self.converter = converter
        self.device = device
        self.iteration = 0

    def update_core(self):
        batch = self._iterators['main'].next()
        in_arrays = self.converter(batch, self.device)

        optimizer = self._optimizers['main']
        
        loss = optimizer.target(in_arrays[0], in_arrays[1])
        optimizer.target.cleargrads()
        loss.backward()
        optimizer.update()


# # Colormap

# In[ ]:


import numpy as np

def get_bit(byte_val, idx):
    return int((byte_val & (1 << idx)) != 0)

def shift_bit(byte_val, idx):
    return byte_val << idx if idx >= 0 else byte_val >> (-idx)

def bitor(a, b):
    return a | b

def make_color_map():
    n = 256
    cmap = np.zeros((n, 3)).astype(np.int32)
    for i in range(0, n):
        d = i - 1
        r,g,b = 0,0,0
        for j in range(0, 7):
            r = bitor(r, shift_bit(get_bit(d, 0), 7 - j))
            g = bitor(g, shift_bit(get_bit(d, 1), 7 - j))
            b = bitor(b, shift_bit(get_bit(d, 2), 7 - j))
            d = shift_bit(d, -3)
        cmap[i, 0] = b
        cmap[i, 1] = g
        cmap[i, 2] = r
    return cmap[1:22]


# # Parameter

# In[ ]:


parser = argparse.ArgumentParser(description='Chainer Fully Convolutional Network')
parser.add_argument('--gpu', '-g', default=0, type=int,
                    help='GPU ID (negative value indicates CPU)')
parser.add_argument('--train_dataset', '-tr', default='dataset', type=str)
parser.add_argument('--target_dataset', '-ta', default='dataset', type=str)
parser.add_argument('--train_txt', '-tt', default='train_txt', type=str)
parser.add_argument('--batchsize', '-b', type=int, default=5,
                    help='batch size (default value is 1)')
parser.add_argument('--initmodel', '-i', default=None, type=str,
                    help='initialize the model from given file')
parser.add_argument('--epoch', '-e', default=300, type=int)
parser.add_argument('--lr', '-l', default=1e-3, type=float)
parser.add_argument('--image_size', default=256, type=int)
parser.add_argument('--classes', default=21, type=int)
args = parser.parse_args()

output_dir = 'good_example'
n_epoch = args.epoch
n_class = args.classes
batchsize = args.batchsize
image_size = args.image_size
train_dataset = '/home/komatsu/work/datasets/VOCdevkit/VOC2012/JPEGImages/'
target_dataset = '/home/komatsu/work/datasets/VOCdevkit/VOC2012/SegmentationClass/'
train_txt = '/home/komatsu/work/datasets/VOCdevkit/VOC2012/ImageSets/Segmentation/trainval.txt'
# train_txt = '/home/komatsu/work/datasets/VOCdevkit/VOC2012/ImageSets/Segmentation/train.txt'
val_txt = '/home/komatsu/work/datasets/VOCdevkit/VOC2012/ImageSets/Segmentation/val.txt'


# In[ ]:


n_class


# # Load data
# url = 'http://host.robots.ox.ac.uk/pascal/VOC/voc2012/VOCtrainval_11-May-2012.tar' # PASCAL VOC2012

# In[ ]:


from chainer import dataset
from PIL import Image
import numpy as np

class VOC2012Dataset(dataset.DatasetMixin):
#     mean_bgr = np.array([104.00698793, 116.66876762, 122.67891434])
    def __init__(self, train_dataset, target_dataset, train_txt):
        self.train_dataset = train_dataset
        self.target_dataset = target_dataset
        
        with open(train_txt,"r") as f:
            name_lines = f.readlines()
        self.fnames = [l.rstrip('\n') for l in name_lines]
        
    def __len__(self):
        return len(self.fnames)


    @classmethod
    def get_image(cls, filename):
        img = Image.open(filename)
        img = cls.crop_data(img, 256)
        x = np.asarray(img, dtype=np.float32).transpose(2, 0, 1)
        x = x/255
        return x
    
    @classmethod
    def get_label(cls, filename):
        label = Image.open(filename)
        label = cls.crop_data(label, 256)
        y = np.asarray(label, dtype=np.int32)
        mask = y==255
        y[mask] = -1
        return y

    def get_example(self, i):
        xpath = os.path.join(self.train_dataset, self.fnames[i]+".jpg")
        ypath = os.path.join(self.target_dataset, self.fnames[i]+".png")
        x = self.get_image(xpath)
        y = self.get_label(ypath)
        return x, y

    @classmethod
    def crop_data(cls, img, size):
        w,h = img.size
        if w < h:
            if w < size:
                img = img.resize((size, size*h//w))
                w, h = img.size
        else:
            if h < size:
                img = img.resize((size*w//h, size))
                w, h = img.size
        img = img.crop((int((w-size)*0.5), int((h-size)*0.5), int((w+size)*0.5), int((h+size)*0.5)))
        return img
    
    @classmethod
    def untransform(cls, img):
        img = img.transpose(1, 2, 0)
        img = img*255
        img = img.astype(np.uint8)
#         img = img[:, :, ::-1]
        return img
    


# In[ ]:


train = VOC2012Dataset(train_dataset, target_dataset, train_txt)
validate = VOC2012Dataset(train_dataset, target_dataset, val_txt)
train_iter = chainer.iterators.SerialIterator(train, batchsize)
valid_iter = chainer.iterators.SerialIterator(validate, batchsize, repeat=False, shuffle=False)
# In[ ]:


from chainer.training import extensions
from chainer import reporter as reporter_module
from chainer import function
from chainer.dataset import convert
class SegEvaluator(extensions.Evaluator):
    default_name = 'valid'
    def __init__(self, iterator, target, n_class,
                 converter=convert.concat_examples, device=None):
        if isinstance(iterator, chainer.dataset.iterator.Iterator):
            iterator = {'main': iterator}
        self._iterators = iterator

        if isinstance(target, chainer.link.Link):
            target = {'main': target}
        self._targets = target

        self.converter = converter
        self.device = device
        self.n_class = n_class
        
    def evaluate(self):
        iterator = self._iterators['main']
        target = self._targets['main']
        xp = target.xp

        if hasattr(iterator, 'reset'):
            iterator.reset()
            it = iterator
        else:
            it = copy.copy(iterator)

        summary = reporter_module.DictSummary()
        confusion_mat = xp.zeros((self.n_class, self.n_class))
        for batch in it:
            observation = {}
            with reporter_module.report_scope(observation):
                imgs, labels = self.converter(batch, self.device)
                with function.no_backprop_mode():
                    loss = model(imgs, labels)
                pred = F.argmax(model.y, axis=1).data
                confusion_mat += self.get_confusion_matrix(pred, labels, self.n_class)
            summary.add(observation)
        
        confusion_mat = cuda.to_cpu(confusion_mat)
        acc = self.compute_metrics(confusion_mat)
        summary.add({'valid/acc': acc[0],
                     'valid/acc_cls': acc[1],
                     'valid/mean_iu': acc[2],
                     'valid/fwavacc': acc[3]})
        return summary.compute_mean()
                                   
    def get_confusion_matrix(self, pred, labels, n_class):
        xp = cuda.get_array_module(pred)

        # ignore unlabeled pixels
        mask = (labels >= 0) & (labels < n_class)
        mat = xp.bincount(pred[mask] + n_class*labels[mask], minlength=n_class**2)
        mat = mat.reshape((n_class, n_class))
        return mat
                                   
    def compute_metrics(self, confusion_mat):
        mat = confusion_mat

        acc = np.diag(mat).sum()/mat.sum()
        with np.errstate(invalid='ignore'):
            cls_acc = np.diag(mat)/mat.sum(axis=1)
            cls_acc = np.nanmean(cls_acc)
            iu = np.diag(mat)/(mat.sum(axis=1) + mat.sum(axis=0) - np.diag(mat))
            meaniu = np.nanmean(iu)     
        freq = mat.sum(axis=1) / mat.sum()
        fwavacc = (freq[freq > 0] * iu[freq > 0]).sum()

        return acc, cls_acc, meaniu, fwavacc               


# # Training

# ## Load model

# In[ ]:


model = Classifier(FCN(n_class))
model.compute_accuracy = False
if args.initmodel:
    serializers.load_npz(args.initmodel, model)
    print("Load initial weight")


# In[ ]:


if args.gpu >= 0:
    chainer.cuda.get_device(args.gpu).use()
    model.to_gpu()
xp = np if args.gpu < 0 else cuda.cupy


# In[ ]:


# Setup optimizer parameters.
optimizer = optimizers.Adam(alpha=args.lr)
optimizer.setup(model)
optimizer.add_hook(chainer.optimizer.WeightDecay(1e-5), 'hook_fcn')


# ## Setup trainer

# In[ ]:


# Set up a trainer
updater = MyUpdater(train_iter, optimizer, device=args.gpu)
trainer = training.Trainer(updater, (n_epoch, 'epoch'), out=output_dir)


# Dump a computational graph from 'loss' variable at the first iteration
# The "main" refers to the target link of the "main" optimizer.
trainer.extend(extensions.dump_graph('main/loss'))

# Take a snapshot at each epoch
snapshot_interval = (20, 'epoch')
trainer.extend(extensions.snapshot(), trigger=snapshot_interval)
trainer.extend(extensions.snapshot_object(
    model, 'model_iter_{.updater.iteration}'), trigger=snapshot_interval)
trainer.extend(extensions.snapshot_object(
    optimizer, 'optimizer_iter_{.updater.iteration}'), trigger=snapshot_interval)

# Evaluator
trainer.extend(SegEvaluator(valid_iter, model, args.classes,device=args.gpu), trigger=(5, 'epoch'))

# Write a log of evaluation statistics for each epoch
trainer.extend(extensions.LogReport())
trainer.extend(extensions.PrintReport(
    ['epoch', 'main/loss', 'valid/acc_cls','valid/mean_iu', 'elapsed_time']))

# Print a progress bar to stdout
trainer.extend(extensions.ProgressBar(update_interval=20))


# In[ ]:


# get_ipython().magic(u'matplotlib inline')
import matplotlib.pyplot as plt
# Plot loss
trainer.extend(extensions.PlotReport(['main/loss', 'valid/main/loss'], x_key='epoch', file_name='loss.png'))
trainer.extend(extensions.PlotReport(['valid/acc', 'valid/acc_cls', 'valid/mean_iu', 'valid/fwavacc'], 
                                     x_key='epoch', file_name='accuracy.png'))


# ## Run

# In[ ]:


resume_fname = False
# resume_fname = os.path.join(output_dir, 'snapshot_iter_52434')

if resume_fname:
    if not os.path.exists(resume_fname):
        raise IOError('Resume file is not exists.')
    print('Load optimizer state from {}'.format(resume_fname))
    chainer.serializers.load_npz(resume_fname, trainer)


# In[ ]:


# %%snakeviz
# Run the training
trainer.run()


# In[ ]:





# # Save weight

# In[ ]:


save_weight_path = os.path.join(output_dir, 'weight')
if not os.path.exists(save_weight_path):
    os.mkdir(save_weight_path)
serializers.save_npz(os.path.join(save_weight_path, 'chainer_fcn.model'), model)
serializers.save_npz(os.path.join(save_weight_path, 'chainer_fcn.optimizer'), optimizer)
print('save weight, ', save_weight_path)

print("end")
